;; Changes all yes/no questions to y/n type
(fset 'yes-or-no-p 'y-or-n-p)

;; shell scripts
(setq-default sh-basic-offset 2)
(setq-default sh-indentation 2)

;; No need for ~ files when editing
(setq create-lockfiles nil)

;; Go straight to scratch buffer on startup
(setq inhibit-startup-message t)

;; auto-restore last active session
(desktop-save-mode 1)

;; copy/paste from and to X-Window
(defun copy-to-clipboard ()
  (interactive)
  (if (display-graphic-p)
      (progn
        (message "Yanked region to x-clipboard!")
        (call-interactively 'clipboard-kill-ring-save)
        )
    (if (region-active-p)
        (progn
          (shell-command-on-region (region-beginning) (region-end) "xsel -i -b")
          (message "Yanked region to clipboard!")
          (deactivate-mark))
      (message "No region active; can't yank to clipboard!")))
  )

(defun paste-from-clipboard ()
  (interactive)
  (if (display-graphic-p)
      (progn
        (clipboard-yank)
        (message "graphics active")
        )
    (insert (shell-command-to-string "xsel -o -b"))
    )
  )

;;; Show file path in mini buffer
(defun show-file-name ()
  "Show the full path file name in the minibuffer."
  (interactive)
  (message (buffer-file-name)))

(global-set-key [f5] 'copy-to-clipboard)
(global-set-key [f6] 'paste-from-clipboard)
(global-set-key [f9] 'show-file-name)

;;; Use external programs on file links
(setq reverse-org-file-apps 
  '(("firefox %s" . "\\.\\(?:xhtml\\|html\\|txt\\|jpg\\|png\\)")
    ("gedit %s" . "\\(?:txt\\|cc\\)")
    ("evince \"%s\"" . "\\.pdf\\'") 
    ("okular \"%s\"" . "\\.pdf\\'")))

(defun new-org-open-at-point (program)
  (interactive
   (list 
    (completing-read "Open with: "
                     reverse-org-file-apps
                     nil t)))
  (let* ((chosen-program (assoc program reverse-org-file-apps))
         (org-file-apps (list (cons (cdr chosen-program) 
                                    (car chosen-program)))))
    (org-open-at-point)))

(global-set-key (kbd "C-z") 'new-org-open-at-point)

;; show hidden in Dired mode
(setq dired-listing-switches "-Alh --group-directories-first")
