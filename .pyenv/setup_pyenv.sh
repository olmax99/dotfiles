pyenv_plugins=(
    jawshooah/pyenv-default-packages
    pyenv/pyenv-virtualenv
    pyenv/pyenv-virtualenvwrapper
    pyenv/pyenv-update
    #aiguofer/pyenv-jupyter-kernel
)

for plugin in "${pyenv_plugins[@]}"; do
    plugin_name=$(echo $plugin | cut -d '/' -f2)
    git clone https://github.com/$plugin $(pyenv root)/plugins/$plugin_name
done
