# Pyenv default plugin

pyenv-default-packages automatically installs the packages listed in the $(pyenv root)/default-packages file every time you successfully install a new version of Python with pyenv install or create a virtualenv with pyenv virtualenv.

## Usage

### 1. Install plugin
```
$ /bin/bash ~/.pyenv/setup_pyenv.sh

```
### 2. Activate plugin
Create a symlink in your $(pyenv root) folder using:
```
$ ln -s ~/.pyenv/default_packages /usr/local/src/github.com/pyenv/pyenv/default-packages

```
